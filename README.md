# dhtstat

un programme qui explore la DHT de BitTorrent pour produire des statistiques

## compilation

    make

nécessite `ocaml` 4.01 et `ocamlfind` ; en l’absence d’`ocamlfind`, le programme
peut être compilé ainsi :

    cp src/* .
    ocamlopt -c -g Util.mli -o Util.cmi
    ocamlopt -c -g Util.ml -o Util.cmx
    ocamlopt -c -g Bencode.mli -o Bencode.cmi
    ocamlopt -c -g Bencode.ml -o Bencode.cmx
    ocamlopt -c -g Main.ml -o Main.cmx
    ocamlopt -g unix.cmxa Util_cstubs.c Util.cmx Bencode.cmx Main.cmx -o dhtstat

mais c’est moins beau.

## exécution

    ./dhtstat

pour une liste des paramètres :

    ./dhtstat --help

le programme écrit des messages de log (en quantité) et produit périodiquement
des statistiques ; par défaut, les deux sont écrits sur la sortie standard, ce
qui se change avec `--log FILE` et `--stat FILE` respectivement ; par exemple,
pour se débarasser des logs :

    ./dhstat --log /dev/null

les statistiques produites sont de cette forme :

    --STATS--
    total:         111    # nombre total de nœuds distincts rencontrés
    timeout:        27    # nœuds qu’on n’a pas réussi à joindre
       ipv6:         0    # parmi eux, nœuds IPv6
    reached:        84    # nœuds contactés avec succès
       ipv6:         0    # parmi eux, nœuds IPv6
    bep32:           3    # nœuds implémentant BEP-32
    bugged:          0    # nœuds bogués
    versions: 29          # liste de toutes les chaînes de version rencontréés, …
        ""          :        18 # … avec leur nombre d’occurrences
        "lt\r\000"  :         1
        "UTb\214"   :         1
        "UTb\022"   :         1
        ...
    --END--

## exemple

quelques résultats obtenus se trouvent dans le dossier `results/`. chaque
fichier a été obtenu à différents horaires, en exécutant simultanément quelques
dizaines d’instances de `dhtstat` et en sommant les résultats obtenus (à l’aide
du script `read_stat.ml`).
