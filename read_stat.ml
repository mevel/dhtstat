#!/usr/bin/ocaml

(*
 * a handy script to handle statistics produced by dhtstat
 *
 * usage:
 *     read_stat.ml FILE1 FILE2…
 * sums the stats contained in all the files and write the resulting stat on
 * standard output ;
 * also writes a summary of statistics (with version strings simplified and
 * grouped together) on standard error.
 *
 *)

let alpha_compare (v1,_) (v2,_) =
  compare (String.uppercase v1) (String.uppercase v2)

type stat =
  {
    total : int ;
    unreachable : int ;
    unreachable_ipv6 : int ;
    reached : int ;
    ipv6 : int ;
    bep32 : int ;
    bugged : int option ;
    versions : (string * int) list ;
  }

let zero =
  {
    total = 0 ;
    unreachable = 0 ;
    unreachable_ipv6 = 0 ;
    reached = 0 ;
    ipv6 = 0 ;
    bep32 = 0 ;
    bugged = Some 0 ;
    versions = [] ;
  }

let sum_options o1 o2 =
  begin match o1, o2 with
  | Some x, Some y -> Some (x + y)
  | _              -> None
  end

let rec merge_versions li1 li2 =
  begin match li1, li2 with
  | [], li
  | li, [] -> li
  | (v1,c1 as x1) :: li1', (v2,c2 as x2) :: li2' ->
      let cmp = alpha_compare x1 x2 in
      if cmp < 0 then
        (v1, c1) :: merge_versions li1' li2
      else if cmp > 0 then
        (v2, c2) :: merge_versions li1 li2'
      else
        (v1, c1+c2) :: merge_versions li1' li2'
  end

let (++) s1 s2 =
  {
    total            = s1.total            + s2.total ;
    unreachable      = s1.unreachable      + s2.unreachable ;
    unreachable_ipv6 = s1.unreachable_ipv6 + s2.unreachable_ipv6 ;
    reached          = s1.reached          + s2.reached ;
    ipv6             = s1.ipv6             + s2.ipv6 ;
    bep32            = s1.bep32            + s2.bep32 ;
    bugged   = sum_options    s1.bugged   s2.bugged ;
    versions = merge_versions s1.versions s2.versions ;
  }

let print output s =
  Printf.fprintf output "--STATS--\n" ;
  Printf.fprintf output "total:   %9i\n" @@ s.total ;
  Printf.fprintf output "timeout: %9i\n" s.unreachable ;
  Printf.fprintf output "   ipv6: %9i\n" s.unreachable_ipv6 ;
  Printf.fprintf output "reached: %9i\n" s.reached ;
  Printf.fprintf output "   ipv6: %9i\n" s.ipv6 ;
  Printf.fprintf output "bep32:   %9i\n" s.bep32 ;
  begin match s.bugged with
  | Some bugged -> Printf.fprintf output "bugged:  %9i\n" bugged
  | None        -> ()
  end ;
  Printf.fprintf output "versions: %i\n" @@ List.length s.versions ;
  List.iter (fun (version, count) ->
      Printf.fprintf output "\t%-12S: %9i\n" version count
    )
    s.versions ;
  Printf.fprintf output "--END--\n%!"

let rec parse_versions input =
  try
    Scanf.fscanf input " %S : %i " @@ fun v count ->
        (v, count) :: parse_versions input
  with Scanf.Scan_failure _ | End_of_file ->
    []

let parse input =
  Scanf.fscanf input "--STATS--\n" () ;
  let total            = Scanf.fscanf input " total : %i "   @@ fun x -> x in
  let unreachable      = Scanf.fscanf input " timeout : %i " @@ fun x -> x in
  let unreachable_ipv6 = Scanf.fscanf input " ipv6 : %i "    @@ fun x -> x in
  let reached          = Scanf.fscanf input " reached : %i " @@ fun x -> x in
  let ipv6             = Scanf.fscanf input " ipv6 : %i "    @@ fun x -> x in
  let bep32            = Scanf.fscanf input " bep32 : %i "   @@ fun x -> x in
  let bugged           =
    try
      Scanf.fscanf input " bugged : %i "  @@ fun x -> Some x
    with Scanf.Scan_failure _ ->
      None
  in
  Scanf.fscanf input " versions : %_i " () ;
  let versions = List.fast_sort alpha_compare @@ parse_versions input in
  Scanf.fscanf input " --END-- " () ;
  {
    total ;
    unreachable ;
    unreachable_ipv6 ;
    reached ;
    ipv6 ;
    bep32 ;
    bugged ;
    versions ;
  }

let rec parse_file input =
  let stat = parse input in
  try
    parse_file input
  with End_of_file ->
    stat

let parse_filename filename =
  let file = open_in filename in
  let stat = parse_file file in
  close_in file ;
  stat

let summarize_version =
  let is_alpha = function
    | 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' -> true
    | _ -> false
  in
  fun v ->
    if String.length v >= 2 && is_alpha v.[0] && is_alpha v.[1] then
      String.uppercase (String.sub v 0 2)
    else
      v

let summarize_versions =
  let rec uniq = function
    | (v1,c1) :: (v2,c2) :: li' when v1 = v2 -> uniq ((v1, c1+c2) :: li')
    | e :: li' -> e :: uniq li'
    | [] -> []
  in
  fun li ->
    li
    |> List.map (fun (v, c) -> summarize_version v, c)
    |> uniq

let (//) x y =
  float x /. float y

let summarize s =
  Printf.eprintf "total: %i\n" s.total ;
  Printf.eprintf "%% unreachable: %.2g\n" @@ s.unreachable // s.total ;
  begin match s.bugged with
  | Some bugged -> Printf.eprintf "%% bugged: %.2g\n" @@ bugged // s.reached
  | None        -> ()
  end ;
  Printf.eprintf "%% ipv6:   %.2g\n" @@ s.ipv6 // s.reached ;
  Printf.eprintf "%% bep32:  %.2g\n" @@ s.bep32 // s.reached ;
  let versions =
    s.versions
    |> summarize_versions
    |> List.fast_sort (fun (c1,v1) (c1,v2) -> compare v2 v1)
  in
  let unknown = try List.assoc "" versions with _ -> 0 in
  let known = s.reached - unknown in
  Printf.eprintf "versions:\n" ;
  Printf.eprintf "\tunknown : %6i (%.2g)\n" unknown (unknown // s.reached) ;
  List.iter (fun (v,c) ->
      if v <> "" then
        Printf.eprintf "\t%-8S: %6i (%.2g)\n" v c (c // known)
    )
    versions ;
  s

let () =
  Sys.argv
  |> Array.to_list
  |> List.tl
  |> List.fold_left (fun acc name -> acc ++ parse_filename name) zero
  |> summarize
  |> print stdout
