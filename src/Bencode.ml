open Util.Op

type value =
  | Int of int64 (* TODO: Big_int? *)
  | String of string
  | List of value list
  | Dict of (string * value) list

let int x = Int x
let string x = String x
let list li = List li
let dict di = Dict (List.fast_sort compare di)

exception Bad_value

let get_int = function
  | Int x -> x
  | _     -> raise Bad_value
let get_string = function
  | String x -> x
  | _        -> raise Bad_value
let get_list = function
  | List li -> li
  | _       -> raise Bad_value
let get_dict = function
  | Dict di -> di
  | _       -> raise Bad_value

let get key = (* can raise Bad_value or Not_found *)
  List.assoc key % get_dict

let mem key = (* can raise Bad_value *)
  List.mem_assoc key % get_dict

module Op =
struct
  let ( $  ) dict key = dict |> get key
  let ( $? ) dict key = dict |> mem key
end

let is_int = function
  | Int x -> true
  | _     -> false
let is_string = function
  | String x -> true
  | _        -> false
let is_list = function
  | List li -> true
  | _       -> false
let is_dict = function
  | Dict di -> true
  | _       -> false

exception Decode_error of string

let rec print output = function
  | Int i -> Printf.bprintf output "%Li" i
  | String s -> Printf.bprintf output "%S" s
  | List li -> Printf.bprintf output "[ %a]" (List.iter % print_elem) li
  | Dict di -> Printf.bprintf output "{ %a}" (List.iter % print_assoc) di
and print_elem  output elem =
  Printf.bprintf output "%a, " print elem
and print_assoc output (key,value) =
  Printf.bprintf output "%S:%a, " key print value

let rec encode output = function
  | Int i ->
      Printf.bprintf output "i%Lie" i
  | String s ->
      Printf.bprintf output "%i:%s" (String.length s) s
  | List li ->
      Printf.bprintf output "l%ae" (List.iter % encode) li
  | Dict di ->
      Printf.bprintf output "d%ae" (List.iter % encode_assoc) di
and encode_assoc output (key,value) =
  Printf.bprintf output "%a%a" encode (String key) encode value

let rec decode input =
  try
    Scanf.bscanf input "i%Lie" int
  with Scanf.Scan_failure _ ->
  try
    string @@ decode_string input
  with Scanf.Scan_failure _ ->
  try
    Scanf.bscanf input "l%re" (Util.iter_scanner decode) list
  with Scanf.Scan_failure _ ->
    Scanf.bscanf input "d%re" (Util.iter_scanner decode_assoc) dict
and decode_string input =
  Scanf.bscanf input "%i:" @@ fun len ->
    let s = Util.scan_n_bytes len input in
    let len' = String.length s in
    if len' < len then
      raise @@ Decode_error
        (Printf.sprintf "%i bytes expected but only %i remaining at end of input" len len') ;
    s
and decode_assoc input =
  Scanf.bscanf input "%r%r" decode_string decode @@ fun key value -> key, value

let decode input =
  try
    decode input
  with Scanf.Scan_failure msg ->
    raise @@ Decode_error msg

let print_s  = Util.sprinter_of_printer print
let encode_s = Util.sprinter_of_printer encode
let decode_s = Util.sscanner_of_scanner decode

(* tests *)
let () =
  let value =
    dict [
      "cow",  string "moo lin";
      "spam", int 42L;
      "list", list [
        string "spam";
        string "eggs";
      ];
    ]
  and benc = "d3:cow7:moo lin4:listl4:spam4:eggse4:spami42ee" in
  assert (benc = encode_s value) ;
  assert (value = decode_s benc)
