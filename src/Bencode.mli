type value = private
  | Int of int64 (* TODO: Big_int? *)
  | String of string
  | List of value list
  | Dict of (string * value) list

val int    : int64                 -> value
val string : string                -> value
val list   : value list            -> value
val dict   : (string * value) list -> value

exception Bad_value

val get_int    : value -> int64
val get_string : value -> string
val get_list   : value -> value list
val get_dict   : value -> (string * value) list
val get : string -> value -> value
val mem : string -> value -> bool

module Op :
sig
  val ( $  ) : value -> string -> value
  val ( $? ) : value -> string -> bool
end

val is_int    : value -> bool
val is_string : value -> bool
val is_list   : value -> bool
val is_dict   : value -> bool

exception Decode_error of string

val print : value Util.printer
val print_s : value -> string

val encode : value Util.printer
val encode_s : value -> string

val decode : value Util.scanner
val decode_s : string -> value
