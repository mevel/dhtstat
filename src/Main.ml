(*
 *  settings
 *)

module Config =
struct
  (* whether to use IPv6 by default: *)
  let ipv6 = false
  (* whether to inspect both IPv{4,6} DHT (requires ‘ipv6’ to be set);
   * in any case, in order to test support for BEP-32, ‘find_node’ queries will
   * ask for IPv4 and IPv6 nodes; if ‘dual_stack’ is not set, only the relevant
   * ones are kept: *)
  let dual_stack = false

  (* bootstrap nodes (found at http://stackoverflow.com/questions/27646569): *)
  let bootstrap_addrs =
    [
      "router.utorrent.com", 6881 ;
      "router.bittorrent.com", 6881 ; (* alias to the previous *)
      "dht.transmissionbt.com", 6881 ;
      "dht.wifi.pps.jussieu.fr", 6881 ; (* dead :/ *)
    ]

  (* default number of nearest nodes to search: *)
  let num_nearest = 20

  (* maximum number of iterations in the search algorithm: *)
  let max_iterations = 20

  (* default numbor of iterations of the search algorithm (-1 is forever): *)
  let num_loops = 1

  (* time to wait for a response (in seconds, -1. is eternity): *)
  let timeout = 1.

  (* size of the buffer receiving a response: *)
  let response_buf_sz = 4096

  (* whether to print debug messages on stderr: *)
  let debug_msgs = false
end


(*
 *  miscellaneous
 *)

open Util.Op
open Bencode.Op

let () = assert (not Config.dual_stack || Config.ipv6)
let ip_proto = if Config.ipv6 then Unix.PF_INET6 else Unix.PF_INET
let ip_size ipv6 = if ipv6 then 16 else 4

let num_loops   = ref Config.num_loops
let num_nearest = ref Config.num_nearest
let stat_file = ref stdout
let log_file  = ref stdout

let debug fmt = Util.debug Config.debug_msgs fmt
let log fmt = Printf.fprintf !log_file fmt


(*
 *  manage identifiers (Id), network addresses (Addr) and node info (Node)
 *)

module Id :
sig
  type id
  val print : out_channel -> id -> unit
  val decode : string -> id
  val encode : id -> string
  val random : unit -> id
  val zero : id (* a special id (to indicate buggy nodes) *)
  val distance : id -> id -> id
  val compare : id -> id -> int
end =
struct
  type id = string (* 20 bytes *)

  let print output =
    String.iter (fun b -> Printf.fprintf output "%02x" @@ Char.code b)

  let random () =
    let id = String.create 20 in
    for i = 0 to 19 do
      id.[i] <- Char.chr @@ Random.int 256
    done ;
    id

  let zero = String.make 20 '\000'

  let decode s =
    assert (String.length s = 20) ;
    s

  external encode : id -> string = "%identity"

  let distance id1 id2 =
    let id = String.create 20 in
    for i = 0 to 19 do
      id.[i] <- Char.chr @@ Char.code id1.[i] lxor Char.code id2.[i]
    done ;
    id

  let compare = compare
end

module Addr :
sig
  type addr = Unix.sockaddr
  val print : out_channel -> addr -> unit
  val decode : ?ipv6:bool -> string -> addr
  val get_ip : addr -> string
  val is_ipv6 : addr -> bool
end =
struct
  type addr = Unix.sockaddr

  let print output = function
    | Unix.ADDR_UNIX s ->
        Printf.fprintf output "%s" s
    | Unix.ADDR_INET (ip, port) ->
        Printf.fprintf output "%s port %i"
          (Unix.string_of_inet_addr ip)
          port

  let decode ?(ipv6=Config.ipv6) bytes =
    let ip_sz = ip_size ipv6 in
    assert (String.length bytes = ip_sz + 2) ;
    let text_ip =
      if ipv6 then
        Printf.sprintf "%x:%x:%x:%x:%x:%x:%x:%x"
          ((Char.code bytes.[0]) * 256 + Char.code bytes.[1])
          ((Char.code bytes.[2]) * 256 + Char.code bytes.[3])
          ((Char.code bytes.[4]) * 256 + Char.code bytes.[5])
          ((Char.code bytes.[6]) * 256 + Char.code bytes.[7])
          ((Char.code bytes.[8]) * 256 + Char.code bytes.[9])
          ((Char.code bytes.[10]) * 256 + Char.code bytes.[11])
          ((Char.code bytes.[12]) * 256 + Char.code bytes.[13])
          ((Char.code bytes.[14]) * 256 + Char.code bytes.[15])
      else
        Printf.sprintf "%i.%i.%i.%i"
          (Char.code bytes.[0]) (Char.code bytes.[1])
          (Char.code bytes.[2]) (Char.code bytes.[3])
      in
    let ip = Unix.inet_addr_of_string text_ip in
    let port = Char.code bytes.[ip_sz] * 256 + Char.code bytes.[ip_sz+1] in
    Unix.ADDR_INET (ip, port)

  let get_ip = function
    | Unix.ADDR_UNIX _       -> ""
    | Unix.ADDR_INET (ip, _) -> Unix.string_of_inet_addr ip

  let is_ipv6 addr =
    String.contains (get_ip addr) ':'
end

module Node :
sig
  type node
  exception Invalid
  val print : out_channel -> node -> unit
  val decode_nodes : ?ipv6:bool -> string -> node list
  val get_id : node -> Id.id
  val get_addr : node -> Addr.addr
  val make : Id.id -> Addr.addr -> node
end =
struct
  type node = Id.id * Addr.addr

  exception Invalid

  let print output (id, addr) = 
    Printf.fprintf output "id = %a, addr = %a" Id.print id Addr.print addr

  let decode_nodes ?(ipv6=Config.ipv6) bytes =
    let node_sz = 20 + ip_size ipv6 + 2 in
    let len = String.length bytes in
    if len mod node_sz <> 0 then
      raise Invalid ;
    assert (len mod node_sz = 0) ;
    let nodes = ref [] in
    for i = 0 to len / node_sz - 1 do
      let id   = Id.decode         (String.sub bytes (i*node_sz) 20)
      and addr = Addr.decode ~ipv6 (String.sub bytes (i*node_sz+20) (node_sz-20)) in
      nodes := (id, addr) :: !nodes
    done ;
    List.rev !nodes

  let get_id   = fst
  let get_addr = snd
  let make id addr = id, addr
end


(*
 *  useful functions about nodes
 *)

let print_node_list nodes =
  List.iter (log "\tnode: %a\n" Node.print) nodes

let remove_duplicate_ip nodes =
  let ip_seen = Hashtbl.create (List.length nodes) in
  let rec remove = function
    | []             -> []
    | node :: nodes' ->
        let ip = node |> Node.get_addr |> Addr.get_ip in
        if Hashtbl.mem ip_seen ip then
          remove nodes'
        else begin
          Hashtbl.add ip_seen ip node ;
          node :: remove nodes'
        end
  in
  remove nodes


(*
 *  stats
 *)

type node_info = {
  node : Node.node ;
  bugged  : bool ;
  version : string ; (* "" means no version provided *)
  ipv6  : bool ;
  bep32 : bool ;
}

let unreachable_nodes = Hashtbl.create 300_000 (* ip → addr *)
let count_unreachable = ref 0
let count_unreachable_ipv6 = ref 0

let reached_nodes = Hashtbl.create 1_000_000 (* ip → node info *)
let count_reached = ref 0
let count_bugged  = ref 0
let count_ipv6    = ref 0
let count_bep32   = ref 0
let count_versions = Hashtbl.create 1024 (* version → count *)

let add_unreachable_node addr =
  let ip = addr |> Addr.get_ip in
  if not @@ Hashtbl.mem unreachable_nodes ip then begin
    log "\tadding unreachable node addr %a\n" Addr.print addr ;
    Hashtbl.add unreachable_nodes ip addr ;
    incr count_unreachable ;
    if Addr.is_ipv6 addr then
      incr count_unreachable_ipv6
  end

let add_node node_info =
  let ip = node_info.node |> Node.get_addr |> Addr.get_ip in
  if not @@ Hashtbl.mem reached_nodes ip then begin
    log "\tadding node addr %a\n" Addr.print @@ Node.get_addr node_info.node ;
    Hashtbl.add reached_nodes ip node_info ;
    incr count_reached ;
    if node_info.bugged then
      incr count_bugged ;
    if node_info.ipv6 then
      incr count_ipv6 ;
    if node_info.bep32 then
      incr count_bep32 ;
    let c =
      try Hashtbl.find count_versions node_info.version with Not_found -> 0 in
    Hashtbl.replace count_versions node_info.version (c + 1)
  end

let dump_stats () =
  let output = !stat_file in
  Printf.fprintf output "--STATS--\n" ;
  Printf.fprintf output "total:   %9i\n" @@ !count_unreachable + !count_reached ;
  Printf.fprintf output "timeout: %9i\n" !count_unreachable ;
  Printf.fprintf output "   ipv6: %9i\n" !count_unreachable_ipv6 ;
  Printf.fprintf output "reached: %9i\n" !count_reached ;
  Printf.fprintf output "   ipv6: %9i\n" !count_ipv6 ;
  Printf.fprintf output "bep32:   %9i\n" !count_bep32 ;
  Printf.fprintf output "bugged:  %9i\n" !count_bugged ;
  Printf.fprintf output "versions: %i\n" @@ Hashtbl.length count_versions ;
  Hashtbl.iter (fun version count ->
      Printf.fprintf output "\t%-12S: %9i\n" version count
    )
    count_versions ;
  Printf.fprintf output "--END--\n%!"

let check ~ping addr response =
  let bugs = ref [] in
  let bugged msg =
    bugs := msg :: !bugs
  in
  let ipv6 = Addr.is_ipv6 addr in
  let id      = ref Id.zero
  and version = ref ""
  and bep32   = ref false in
  (* checking root *)
  if not (response |> Bencode.is_dict) then
    bugged "response is not a dictionary"
  else begin
    (* getting version *)
    begin try version := response $ "v" |> Bencode.get_string with _ -> () end ;
    (* checking r *)
    if not (response $? "r") then
      bugged "missing field \"r\""
    else begin
      let r = response $ "r" in
      if not (r |> Bencode.is_dict) then
        bugged "\"r\" is not a dictionary"
      else begin
        (* checking r.id *)
        if not (r $? "id") then
          bugged "missing field \"r.id\""
        else begin
          let r_id = response $ "r" $ "id" in
          if not (r_id |> Bencode.is_string) then
            bugged "field \"r.id\" is not a string"
          else begin
            let id_str = r_id |> Bencode.get_string in
            if String.length id_str <> 20 then
              bugged "field \"r.id\" has wrong size"
            else
              id := Id.decode id_str
          end
        end ;
        if not ping then begin
          (* checking r.nodes *)
          if not (r $? "nodes") then
            bugged "missing field \"r.nodes\""
          else begin
            let r_nodes = r $ "nodes" in
            if not (r_nodes |> Bencode.is_string) then
              bugged "field \"r.nodes\" is not a string"
            else begin
              let nodes_str = r_nodes |> Bencode.get_string in
              if String.length nodes_str mod 26 <> 0 then
                bugged "field \"r.nodes\" has wrong size"
            end
          end ;
          (* testing BEP-32 *)
          bep32 := r $? "nodes6" ;
          (* checking r.nodes6 *)
          if ipv6 && not !bep32 then
            bugged "missing field \"r.nodes6\""
          else if !bep32 then begin
            let r_nodes6 = r $ "nodes6" in
            if not (r_nodes6 |> Bencode.is_string) then
              bugged "field \"r.nodes6\" is not a string"
            else begin
              let nodes6_str = r_nodes6 |> Bencode.get_string in
              if String.length nodes6_str mod 38 <> 0 then
                bugged "field \"r.nodes6\" has wrong size"
            end
          end ;
        end (* the query was ‘find_node’ *)
      end (* r is a dict *)
    end (* r exists *)
  end (* root is a dict *) ;
  log "\tversion = %S\n" !version ;
  if !bep32 then
    log "\tsupports BEP-32\n" ;
  List.iter (log "\tbug: %s\n") @@ List.rev !bugs ;
  let node = Node.make !id addr in
  {
    node ;
    bugged  = !bugs <> [] ;
    version = !version ;
    ipv6 ;
    bep32   = !bep32 ;
  }


(*
 *  DHT implementation (queries, responses & iterative search)
 *)

let () = Random.self_init ()

let own_id = Id.encode @@ Id.random ()

let gen_tag =
  let n = ref 0 in
  fun () -> incr n ; Printf.sprintf "%03x" (!n mod 4096)

let query ?(tag=gen_tag()) query_method args =
  let open Bencode in
  dict [
    "t", string tag ;
    "y", string "q" ;
    "q", string query_method ;
    "a", dict @@
      ("id", string own_id) :: args
  ]

let ping_query ?tag () =
  query ?tag "ping" []

let find_query ?tag target_id =
  query ?tag "find_node" Bencode.([
      "target", string (Id.encode target_id) ;
      "want", list [ string "n4" ; string "n6" ] ;
    ])

let rec sendto sock data off len opts addr ~trials =
  let rec send n =
    if n = 0 then
      0
    else
      begin try
        Unix.sendto sock data off len opts addr
      with Unix.Unix_error (_, _, _) -> (* ENOMEM *)
        Unix.sleep 2 ;
        send (n - 1)
      end
  in
  send trials

let request sock addr query =
  let benc = Bencode.encode_s query in
  let len = String.length benc in
  debug "dest addr = %a\n" Addr.print addr ;
  debug "query = %s\n" @@ Bencode.print_s query ;
  let sent_len = sendto sock benc 0 len [] addr ~trials:3 in
  debug "sent len = %i / %i\n" sent_len len ;
  sent_len = len

(* responses already received, indexed by their transaction tag: *)
let stored_responses = Hashtbl.create 16

let receive sock =
  let readable, _, _ = Unix.select [sock] [] [] Config.timeout in
  if readable = [] then begin (* timeout reached *)
    debug "ERR: timeout\n" ;
    None
  end
  else begin (* sock is ready to be read *)
    let buf = String.create Config.response_buf_sz in
    let recv_len, addr' = Unix.recvfrom sock buf 0 Config.response_buf_sz [] in
    debug "recv len = %i\n" recv_len ;
    debug "recv addr = %a\n" Addr.print addr' ;
    let benc' = String.sub buf 0 recv_len in
    begin try
      let msg = Bencode.decode_s benc' in
      debug "recv msg = %s\n" @@ Bencode.print_s msg ;
      Some msg
    with Bencode.Decode_error errmsg ->
      debug "ERR: bencode decoding failed: %s\n" errmsg ;
      None
    end
  end

let rec receive_response sock tag =
  begin try
    let response = Hashtbl.find stored_responses tag in
    Hashtbl.remove stored_responses tag ;
    Some response
  with Not_found ->
    begin match receive sock with
    | Some msg ->
        let msg_tag  = try msg $ "t" |> Bencode.get_string with _ -> ""
        and msg_type = try msg $ "y" |> Bencode.get_string with _ -> "" in
        begin match msg_type with
        | "q" -> debug "dropped query\n"
        | "r" -> debug "received response (expected? %b)\n" (msg_tag = tag)
        | "e" -> debug "dropped error message\n"
        | _   -> debug "ERR: dropped message of invalid type %S!\n" msg_type
        end ;
        if msg_tag = tag && msg_type = "r" then begin
          Some msg
        end
        else begin
          if msg_type = "r" then
            Hashtbl.add stored_responses tag msg ;
          receive_response sock tag
        end
    | None ->
        None
    end
  end

let request_response sock addr query =
  if request sock addr query then begin
    let tag = try query $ "t" |> Bencode.get_string with _ -> ""
    and met = try query $ "q" |> Bencode.get_string with _ -> "" in
    let ping = met = "ping" in
    let result = receive_response sock tag in
    begin match result with
    | Some response ->
        let info = check ~ping addr response in
        add_node info ;
        if info.bugged then None else Some response
    | None ->
        add_unreachable_node addr ;
        None
    end
  end
  else
    None

let request_closer_nodes sock addr target_id =
  begin match request_response sock addr (find_query target_id) with
  | Some response ->
      let nodes =
        if not Config.ipv6 || Config.dual_stack then
          begin try
            response $ "r" $ "nodes" |> Bencode.get_string
            |> Node.decode_nodes ~ipv6:false
          with Bencode.Bad_value | Not_found | Node.Invalid ->
            []
          end
        else
          []
      and nodes6 =
        if Config.ipv6 || Config.dual_stack then
          begin try
            response $ "r" $ "nodes6" |> Bencode.get_string
            |> Node.decode_nodes ~ipv6:true
          with Bencode.Bad_value | Not_found | Node.Invalid ->
            []
          end
        else
          []
      in
      nodes6 @ nodes
  | None ->
      []
  end

let log_request_closer_nodes sock addr target_id =
  log "requesting %a…\n" Addr.print addr ;
  let nodes = request_closer_nodes sock addr target_id in
  print_node_list nodes ;
  nodes

let log_search_k_nearest k sock target_id =
  let cmp node1 node2 =
    Id.compare
      (Id.distance target_id @@ Node.get_id node1)
      (Id.distance target_id @@ Node.get_id node2)
  in
  let insert =
    Util.list_insert_uniq cmp
  in
  let request_to_node node =
    begin match log_request_closer_nodes sock (Node.get_addr node) target_id with
    | [] -> []
    | n  -> node :: n (* we keep the node itself, unless it fails to reply *)
    end
  in
  let step nodes =
    List.fold_right (List.fold_right insert % request_to_node) nodes []
    |> remove_duplicate_ip
    |> Util.list_truncate k
  in
  let stop step_count li li' =
    step_count = Config.max_iterations
    || List.map Node.get_id li = List.map Node.get_id li'
  in
  let rec loop step_count li =
    log "\nstep %i: %i nodes\n" step_count (List.length li) ;
    print_node_list li ;
    log "\n%!" ;
    let li' = step li in
    if stop step_count li li' then
      li'
    else
      loop (step_count+1) li'
  in
  loop 0


(*
 *  DNS resolution
 *)

(* using ‘gethostbyname’ (deprecated, IPv4-only, but simpler): *)
(*
let log_resolve_hostname (hostname, port) =
  log "resolving %s…%!" hostname ;
  begin match try Some (Unix.gethostbyname hostname) with Not_found -> None
  with
  | Some host ->
      let ip = host.Unix.h_addr_list.(0) in
      let addr = Unix.ADDR_INET (ip, port) in
      log " %a\n" Addr.print addr ;
      Some addr
  | None ->
      log " host not found\n" ;
      None
  end
*)
(* using ‘getaddrinfo’ (IPv4 and IPv6): *)
let log_resolve_hostname (hostname, port) =
  log "resolving %s…\n" hostname ;
  begin match
    Unix.getaddrinfo hostname (string_of_int port)
      [ Unix.AI_SOCKTYPE Unix.SOCK_DGRAM ; Unix.AI_FAMILY ip_proto ]
  with
  | { Unix.ai_addr = addr ; _ } :: _ ->
      log "\taddr = %a\n" Addr.print addr ;
      Some addr
  | [] ->
      log "\thost not found\n" ;
      None
  end

let log_resolve_addr_to_node sock addr =
  log "pinging %a…\n" Addr.print addr ;
  begin match request_response sock addr (ping_query ()) with
  | Some response ->
      let id = response $ "r" $ "id" |> Bencode.get_string |> Id.decode in
      log "\tnode id = %a\n" Id.print id ;
      Some (Node.make id addr)
  | None ->
      log "\tfailure\n" ;
      None
  end


(*
 *  main program
 *)

(* command-line options: *)
let options =
  let open Arg in
  align [
    "--loop",    Set_int num_loops,
      "INTEGER   number of iterations of the search algorithm to perform"
      ^ " (default: -1 to run forever)" ;
    "-k",        Set_int num_nearest,
      "INTEGER   number of nearest nodes to search"
      ^ " (default: " ^ string_of_int !num_nearest ^ ")";
    "--log",     String ((:=) log_file  % open_out),
      "FILENAME  log file (default: stdout)" ;
    "--stat",    String ((:=) stat_file % open_out),
      "FILENAME  stat file (default: stdout)" ;
  ]

(* command-line documentation: *)
let usage =
  Printf.sprintf
    ( "usage:\n"
      ^^ "\t%s [OPTION …]\n"
      ^^ "options:" )
    Sys.argv.(0)

let () =
  Arg.parse options (fun a -> raise @@ Arg.Bad ("bad parameter: " ^ a)) usage ;
  (* socket creation *)
  let sock = Unix.socket ip_proto Unix.SOCK_DGRAM 0 in
  Util.setsockopt_dontfrag sock ;
  if Config.ipv6 && not Config.dual_stack then
    Unix.setsockopt sock Unix.IPV6_ONLY true ;
  (* bootstrapping *)
  let k = !num_nearest in
  let bootstrap_nodes =
    Config.bootstrap_addrs
    |> Util.list_map_filter log_resolve_hostname
    |> Util.list_map_filter (log_resolve_addr_to_node sock)
  in
  let nodes = ref bootstrap_nodes in
  (* exploring the DHT *)
  while !num_loops <> 0 && !nodes <> [] do
    let target_id = Id.random () in
    nodes := log_search_k_nearest k sock target_id !nodes ;
    log "\nfound %i / %i nodes nearest from %a:\n" (List.length !nodes) k
      Id.print target_id ;
    print_node_list !nodes ;
    log "\n%!" ;
    dump_stats () ;
    if !num_loops > 0 then
      decr num_loops
  done ;
  Unix.close sock
