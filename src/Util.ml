module Op =
struct
  let (%) g f x = g @@ f x
end

open Op

let rec list_insert_uniq cmp e = function
  | e' :: li' when cmp e e' > 0 -> e' :: list_insert_uniq cmp e li'
  | e' :: li' when cmp e e' = 0 -> e' :: li'
  | li                          -> e :: li

let rec list_truncate k = function
  | e :: li' when k > 0 -> e :: list_truncate (k-1) li'
  | _                   -> []

let list_map_filter f li =
  List.fold_right (fun x li ->
      begin match f x with
      | Some y -> y :: li
      | None   -> li
      end
    )
    li
    []


let debug cond =
  (if cond then Printf.kfprintf flush else Printf.ifprintf) stderr

type 'a printer = Buffer.t -> 'a -> unit
type 'a scanner = Scanf.Scanning.in_channel -> 'a

let sprinter_of_printer printer x =
  let buf = Buffer.create 256 in
  printer buf x ;
  Buffer.contents buf

let sscanner_of_scanner scanner =
  scanner % Scanf.Scanning.from_string

let scan_n_bytes n input =
  let fmt_s = Printf.sprintf "%%%i[\000-\255]" n in
  let fmt = Scanf.format_from_string fmt_s "%s" in
  Scanf.bscanf input fmt @@ fun s -> s

let iter_scanner scanner =
  let rec iter input =
    begin match
      begin try Some (scanner input) with
      | End_of_file
      | Scanf.Scan_failure _ -> None
      end
    with
    | None   -> []
    | Some x -> x :: iter input
    end
  in iter

external _setsockopt_dontfrag : Unix.file_descr -> bool = "setsockopt_dontfrag"
external _get_errno : unit -> int = "get_errno"

let setsockopt_dontfrag sock =
  if not @@ _setsockopt_dontfrag sock then
    raise @@ Unix.Unix_error (Unix.EUNKNOWNERR (_get_errno ()), "setsockopt_dontfrag", "")
