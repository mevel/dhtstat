module Op :
sig
  val ( % ) : ('a -> 'b) -> ('c -> 'a) -> 'c -> 'b
end

val list_insert_uniq : ('a -> 'a -> int) -> 'a -> 'a list -> 'a list
val list_truncate : int -> 'a list -> 'a list
val list_map_filter : ('a -> 'b option) -> 'a list -> 'b list

val debug : bool -> ('a, out_channel, unit) format -> 'a

type 'a printer = Buffer.t -> 'a -> unit
type 'a scanner = Scanf.Scanning.in_channel -> 'a

val sprinter_of_printer : 'a printer -> 'a -> string
val sscanner_of_scanner : 'a scanner -> string -> 'a

val scan_n_bytes : int -> string scanner

val iter_scanner : 'a scanner -> 'a list scanner

val setsockopt_dontfrag : Unix.file_descr -> unit
